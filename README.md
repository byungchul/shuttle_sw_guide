# Suhttle 운전자 모니터링 메인 화면
![Alt text](./images/shuttle_sw_guide.001.png "Shuttle Monitoring GUI")

# 기능 설명 
* 1\. Lidar 전/후방 3channel 표시, Radar 차량 양끝 모서리 4대(전방2, 후방2) 물체 검출 표시
    * 1.1\. LiDAR 전/후방 표시(Hz)
    * 1.2\. Radar 전방2/후방2 표시(Hz)
    * 1.3\. CPU 8core 점유율, 차량 핸들각(deg), 차량 Heading각(deg)
    * 1.4\. 차량 Speed(km/s), 차량 RPM(rpm), 차량 선각속도(m/s^2), 차량 각가속도(m/s^2)
    * 1.5\. Radar 전방2/후방2에서 검출된 상세 정보표시(거리,...)
* 2\. AVM 화면 표시 
    * 2.1\. AVM 표시 (Hz)
    * 2.2\. 메모리 점유율, 저장용량, WIFI/LTE 사용용량 
* 3\. 전방 화면 표시 
    * 3.1\. 전방 화면 표시 (Hz)
    * 3.2\. 전방 인지 표시 (Count)
* 4\. 후방 화면 표시 
    * 2.1\. 후방 화면 표시 (Hz)
    * 2.2\. 후방 인지 표시 (Count)
* 5\. 모든 센서 기록 버튼 
* 6\. 차량 정보(위치,...) 서버로 전송 버튼 
* 7\. 차량 소프트웨어 프로세스 정보 
* 1,2,3,4번 화면 오른쪽 클릭시 전체 화면으로 변경됨.

# 원격 접속 방법
* 1\. 차량 내 WIFI접속
    * SSID     : KATECH
    * 비밀번호 : 1234567890
* 2\. 태블릿의 VNC 클라이언트 실행
    * 접속 주소 : 10.42.0.1:5900
    * 접속 비밀번호 : 0000
* 3\. 접속

# Shuttle 운전자 모니터링 소프트웨어 구조
![Alt text](./images/sw_achitecture.png "Shuttle s/w Architecture")

# Shuttle 운전자 모니터링 앱 설치(개발환경 포함)
* HW: ADLINK(MXC-6401D/M8G) + Nvidia GTX 1070 8G
* OS: Ubuntu 16.04.6 LTS
* CUDA: 10.0
* Nvidia Driver Ver.: 410.93
* SW(Frameworks): ROS1, openframeworks(OF) 0.9.8

---

# 실행 방법
* ADLINK 부팅후 자동 실행 상태에서
```bash
#자동 실행된 프로세스
pm2 list
#프로세스 모니터링 표시 
pm2 monit
#프로세스 로그표시 
pm2 log
#모든 Process 종료
pm2 stop all
#모든 프로세스 재시작
pm2 restart all
```
---

## 설치 순서
1. Cuda & Driver
2. ROS1
3. OF 0.9.8
4. Caffe
5. PM2

---
### Install the Cuda & Driver

#### Disable nouveau
```
sudo vi /etc/modprobe.d/blacklist-nouveau.conf
blacklist nouveau
options nouveau modeset=0
```

#### Update initramfs
```
sudo update-initramfs -u
reboot
```

#### Stop lightdm & Install Nvidia driver
```
sudo apt install build-essential
sudo service lightdm stop (no need to install in ubuntu 18.04)
sudo ./NVIDIA-Linux-x86_64-410.93.run, select all NO
```
#### Install cuda & bashrc
```
sudo ./cuda_10.0.130_410.48_linux.run
vi ~/.bashrc
export PATH=$PATH:/usr/local/cuda-10.0/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda-10.0/ib64
nvidia-smi
```

#### Install vscode(option)
[https://code.visualstudio.com/docs/setup/linux]

#### Clone repository for the shuttle app
```
git clone https://byungchul@bitbucket.org/aspringcloud/katech_songpd.git
git clone -b adlink https://byungchul@bitbucket.org/byungchul/of.git 
git clone -b 0.2.4.2_MAXCRUZ https://byungchul@bitbucket.org/aspringcloud/springgo_router.git
git clone https://byungchul@bitbucket.org/aspringcloud/acan_bridge.git
git clone -b 0.1 https://byungchul@bitbucket.org/aspringcloud/web_video_server.git
git clone https://byungchul@bitbucket.org/aspringcloud/shuttle_launcher.git
git clone https://byungchul@bitbucket.org/aspringcloud/katech_remoteconnectserver.git
git clone https://byungchul@bitbucket.org/aspringcloud/kvaser_interface.git
git clone https://byungchul@bitbucket.org/aspringcloud/pr2_computer_monitor.git
git clone https://byungchul@bitbucket.org/aspringcloud/video_stream_opencv.git
```
---
### Install the ROS1
[http://wiki.ros.org/kinetic/Installation/Ubuntu]
```
sudo apt-get install ros-kinetic-desktop

```
---
### Install the OF 0.9.8
#### install, generate, build
```
cd OF/scripts/linux/ubuntu
sudo ./install_dependencies.sh
remove about mesa in install_dependencies.sh
cd OF/scripts/linux
./compileOF.sh -j3
```
#### catkin make
```
rosdep install --from-paths src --ignore-src -r -y
sudo apt install sysstat // for pr2_robot
sudo apt-get install ros-kinetic-pcl-ros
sudo apt-get install ros-kinetic-pcl-conversions
sudo apt-get install ros-kinetic-can-msgs
sudo apt-get install ros-kinetic-gps-common
sudo apt-add-repository ppa:jwhitleyastuff/linuxcan-dkms // for kvaser
sudo apt update && sudo apt install -y linuxcan-dkms
sudo apt install ros-kinetic-perception // for nodelet image_proc/resize
sudo apt install ros-kinetic-py-trees // for router
catkin_make
```
#### Relink mesa/libGL to libGL.so.1.7.0
```
/usr/bin/ld: cannot find -lGL
pkg-config --list-all
pkg-config --libs gl
bcc@bcc-MXC6400:/usr/lib/x86_64-linux-gnu$ sudo ln -s libGL.so.1.7.0 libGL.so
lrwxrwxrwx 1 root root      13  6월 15  2018 libGL.so -> mesa/libGL.so TO
lrwxrwxrwx 1 root root      14  4월  9 13:46 libGL.so -> libGL.so.1.7.0
```
---
### Install the Caffe
* https://github.com/weiliu89/caffe/blob/ssd/docker/standalone/gpu/Dockerfile
```
cp deploy_.prototxt VGG_VOC0712_SSD_300x300_iter_120000.caffemodel ~/katech_songpd/ssd/models/SSD_Model/
cmake -DUSE_CUDNN=1 ..
make
```
---
### Install the PM2
* http://pm2.keymetrics.io

---
## Troubleshooting:
* unable to locate package libgl1-mesa-dev
* answer is No
```
sudo apt-get install ros-kinetic-pcl-ros
sudo apt-get install ros-kinetic-pcl-conversions
sudo apt-get install ros-kinetic-can-msgs
sudo apt-get install ros-kinetic-gps-common
```
* fatal error: canlib.h: No such file or directory compilation terminated.
* https://github.com/astuff/kvaser_interface
```
sudo apt-add-repository ppa:jwhitleyastuff/linuxcan-dkms
```
* answer is yes
```
sudo apt update && sudo apt install -y linuxcan-dkms
```

* pr2_computer_monitor/pr2_computer_monitor/scripts$ chmod +x *.py
```
sudo apt install ros-kinetic-perception // for nodelet image_proc/resize
sudo apt install ros-kinetic-py-trees // for router
copy config.json to /hoem/bcc
edit springgo_router/launch/publisher.launch
```
* Error display window when PC boot
```
vi /etc/defalt/apport
1 -> 0
```